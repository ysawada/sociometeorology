# sociometeorology

Code for a socio-hydrological model to analyze the impact of cry wolf effects on the efficiency of flood early warning systems in:

Impact of cry wolf effects on social preparedness and efficiency of flood early warning systems, preprint in Hydrological and Earth System Sciences, https://doi.org/10.5194/hess-2021-497


