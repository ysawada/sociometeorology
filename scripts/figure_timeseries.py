#/bin/python3
#
# analyzing timeseries of Sociomet models
#

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import numpy.ma as ma
import os
import math




path_to_data = '/home/yohei/research/sociomet/output/monte_carlo/'

df1 = np.loadtxt(path_to_data+'timeseriesK')
preparedness = df1[:,0]
trust = df1[:,1]
risk_cog = df1[:,2]
falsepositive = df1[:,3]
falsenegative= df1[:,4]
truepositive = df1[:,5]

X  = np.arange(1000)
plt.figure(figsize=(8,8))
plt.bar(X,falsepositive*10,label='false positive', color = 'b', width=2.0)
plt.bar(X,falsenegative,label='false negative', color = 'r', width=2.0)
plt.bar(X,truepositive,label='true positive', color = 'limegreen', width=2.0)
plt.plot(X,preparedness*0.5,label='experience',color = 'purple')
plt.plot(X,trust*0.5, label='trust',color='pink')
plt.plot(X,risk_cog, label='preparedness',color='black')
plt.xlabel("time",fontsize=16)
plt.tick_params(labelsize=16)
#plt.xlim(375,500) # all time series for revision
plt.ylim(0,1.0) # all time series for revision
plt.legend()
plt.savefig('./fig/FigureS1b_Ktimeseries_all.png')
plt.show()
