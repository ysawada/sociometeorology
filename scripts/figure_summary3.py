#/bin/python3
#
# analyzing summary statistics of Sociomet models
#

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import numpy.ma as ma
import os
import math
from sklearn.gaussian_process import kernels as sk_kern
from sklearn.gaussian_process import GaussianProcessRegressor




path_to_data = '/home/yohei/research/sociomet/output/monte_carlo/'

df1 = pd.read_csv(path_to_data+'exp3_2_2_1_results_mc.txt',sep=',')
df2 = pd.read_csv(path_to_data+'exp3_2_2_2_results_mc.txt',sep=',')
df3 = pd.read_csv(path_to_data+'exp3_2_2_3_results_mc.txt',sep=',')
#df1 = pd.read_csv(path_to_data+'exp1_1_results_mc.txt',sep=',')
#df2 = pd.read_csv(path_to_data+'exp1_4_results_mc.txt',sep=',')
xx = np.zeros((100,1)) 
xx[:,0] = np.linspace(0,1,100)

plt.figure(figsize=(8,8))

x = np.zeros((250,1)) 
x[:,0] = df1['prob_thr'].values
y = df1['av_trust'].values
clf = GaussianProcessRegressor(
            kernel=None,
            alpha=1e-10,
            optimizer='fmin_l_bfgs_b',
            n_restarts_optimizer=1,
            normalize_y=False)
clf.fit(x,y)
clf.kernel_

pred_mean,pred_std = clf.predict(xx,return_std=True)
plt.scatter(x,y,s=10,alpha=0.4)
plt.plot(xx[:,0],pred_mean)

x = np.zeros((250,1)) 
x[:,0] = df2['prob_thr'].values
y = df2['av_trust'].values
clf = GaussianProcessRegressor(
            kernel=None,
            alpha=1e-10,
            optimizer='fmin_l_bfgs_b',
            n_restarts_optimizer=1,
            normalize_y=False)
clf.fit(x,y)
clf.kernel_

pred_mean,pred_std = clf.predict(xx,return_std=True)
plt.scatter(x,y,s=10,alpha=0.4)
plt.plot(xx[:,0],pred_mean)

x = np.zeros((250,1)) 
x[:,0] = df3['prob_thr'].values
y = df3['av_trust'].values
clf = GaussianProcessRegressor(
            kernel=None,
            alpha=1e-10,
            optimizer='fmin_l_bfgs_b',
            n_restarts_optimizer=1,
            normalize_y=False)
clf.fit(x,y)
clf.kernel_

pred_mean,pred_std = clf.predict(xx,return_std=True)
plt.scatter(x,y,s=10,alpha=0.4)
plt.plot(xx[:,0],pred_mean)
plt.tick_params(labelsize=16)
plt.xlabel("predefined probability threshold",fontsize=16)
plt.ylabel("Trust",fontsize=16)
plt.ylim(0.0,1.0)
plt.savefig('./Figure4h_techinaccurate.png')

plt.show()
