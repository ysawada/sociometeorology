# Add figure
import numpy as np
import pandas as pd
from math import isclose

import modules as mod
import functions as fun
import os
import matplotlib.pyplot as plt

#girons の figure.6を作るコードのメモ。 main_add の experiment2 のメモの所に挿入。

        fig = plt.figure(figsize = (12, 5))

        # 目盛り線方向を内側にする
        plt.rcParams["xtick.direction"] = "in"
        plt.rcParams["ytick.direction"] = "in"

        # values defining
        a1 = self.mc_stats.loc[:,'mag_thr']
        a2 = self.mc_stats.loc[:,'prob_thr']
        a3 = self.mc_stats.loc[:,'res_dmg']
        b = self.mc_stats.loc[:,'half_life']
        value = self.mc_stats.loc[:,'rel_loss']

        # mag_thr - half-time graph
        ax1 = fig.add_subplot(1,3,1)

        ax1_g = ax1.scatter(a1, b, c = value, cmap = 'RdYlBu_r', s = 2, vmin=0.3, vmax=1.1)
        ax1.set_xlim(0.35, 0.8)
        ax1.set_ylim(1, 100)
        ax1.set_xlabel("Magnitude threshold")
        ax1.set_ylabel("Memory half-life(yrs)")

        ax1.set_yscale('log')

        ax1.set_xticks([0.4,0.6,0.8])

        # pro_thr - half-time graph
        ax2 = fig.add_subplot(1,3,2)

        ax2_g = ax2.scatter(a2, b, c = value, cmap = 'RdYlBu_r', s = 2)
        ax2.set_xlim(0.2, 0.8)
        ax2.set_ylim(1, 100)
        ax2.set_xlabel("Probability threshold")

        ax2.set_yscale('log')

        ax2.set_xticks([0.2, 0.4, 0.6, 0.8])

        # res_dmg - half-time graph
        ax3 = fig.add_subplot(1,3,3)

        ax3_g = ax3.scatter(a3, b, c = value, cmap = 'RdYlBu_r', s = 2)
        ax3.set_xlim(0.1, 0.9)
        ax3.set_ylim(1,100)
        ax3.set_xlabel("Residual damage")

        ax3.set_yscale('log')

        ax3.set_xticks([0.2, 0.4, 0.6, 0.8])

        # color bar (relative loss)
        cbar = plt.colorbar(ax1_g, ticks =[0.4, 0.6, 0.8, 1.0])
        cbar.set_label('Relative loss')

        # save graph
        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        fig.savefig(path_data + "Figure 6 (model efficiency).png")

# Girons の figure.4 を作るコード （SingleRunに挿入） 。
# main_add のsingle_run, self.run(datafile)の真下に挿入。

        # experiment1 add graph(single run)
        """add graphx
        """
        # time_prep txtfile (add)
        ts = fun.load_data(datafile).T
        ts2 = np.append(ts,[self.preparedness_ls],axis=0)
        ts3 = np.append(ts2, [self.warning_loss_ls], axis=0)


        ts4 = pd.DataFrame(ts3)
        ts5 = ts4.T


        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        ts5.to_csv(path_data + filename_data, index_label='Index')

        # loss_prep txtfile (add)
        lp = self.warning_loss_ls
        lp2 = pd.DataFrame(lp).T
        lp3 = np.append(lp2, [self.preparedness_ls], axis=0)
        lp4 = pd.DataFrame(lp3).T

        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        lp4.to_csv(path_data + filename_data2, index_label='Index')

        # loss_prep graph (all 1000 times)

        X = np.arange(1000)
        Y1 = self.warning_loss_ls
        Y2 = self.preparedness_ls

        #loss_prep graph (only 100 times)

        U = np.arange(500,600)
        V1 = self.warning_loss_ls[500:600]
        V2 = self.preparedness_ls[500:600]

        fig = plt.figure()

        plt.bar(U, V1, label = 'flood damage', color = 'tab:orange')
        plt.bar(U, V2, label = 'prep_level', color = 'tab:gray', alpha = 0.3)
        plt.legend()
        plt.title("Evolution of preparedness")
        plt.xlabel("time(yrs)")
        plt.ylabel("Damage, Preparedness level")

        # save graph
        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        fig.savefig(path_data + "loss_prep graph.png")


        # end
        """end
        """

# trust * damageの時系列変化を見る図。 Gironsの figure.4のtrust-ver.を作る。
# 上記の prep.verと同じもの。挿入位置は同じ。

# trust_damage - time_series txtfile
ts = fun.load_data(datafile).T
ts2 = np.append(ts, [self.trust_ls], axis = 0)
ts3 = np.append(ts2, [self.warning_loss_ls], axis = 0)

ts4 = pd.DataFrame(ts3)
ts5 = ts4.T

path_data = os.getcwd() + '/../config'
if not os.path.exists(path_data):
    os.makedirs(path_data)

ts5.to_csv(path_data + file_name_data, index_label='Index')

#loss_trust txtfile
#あとで写す。

#1000年全部表示
X = np.arange(1000)
Y1 = self.warning_loss_ls
Y2 = self.trust_ls

#500-600年だけ表示
U = np.arange(500, 600)
V1 = self.warning_loss_ls[500:600]
V2 = self.trust_ls[500:600]

fig = plt.figure()

plt.bar(X, Y1, label = 'flood damage', color = 'tab:orange')
plt.bar(X, Y2, label = 'trust_level', color = 'tab:gray', alpha = 0.3)
plt.legend()
plt.title("Evolution of trust")
plt.xlabel("time(yrs)")
plt.ylabel("Damage, trust level")

path_data = os. getcwd() + '/../config/'
if not os.path.exists(path_data):
    os.makedirs(path_data)

fig.savefig(path_data + "loss_trust graph.png")


# damage - trust グラフ。tpやfnを色分けで示す。

#PrepFewsの__init__に入れるやつ。
self.fp_ls = []
self.fn_ls = []
self.tp_ls = []
#ここまで。

# これはPrepFewsのrunのところに入れるもの。３つの警報種類ごとにdamageをいれる。
# tnは何も起こらないのでいらない。
if warning_outcome == 'act_false positive':
    self.fp_ls.append(warning_loss)

elif warning_outcome == 'noact_false positive':
    self.fp_ls.append(warning_loss)

else:
    self.fp_ls.append(0)

if warning_outcome == 'false negative':
    self.fn_ls.append(warning_loss)

else:
    self.fp_ls.append(0)

if warning_outcome == 'act_true positive':
    self.tp_ls.append(warning_loss)

elif warning_outcome == 'noact_true positive':
    self.tp_ls.append(warning_loss)

else:
    self.tp_ls.append(0)

#ここまでPrepFewsのrunに挿入。

#ここからsingle_runに挿入。

X = np.arange(1000)
Yfp = self.fp_ls
Yfn = self.fn_ls
Ytp = self.tp_ls
Y = self.trust_ls

U = np.arange(500, 600)
Vfp = self.fp_ls[500:600]
Vfn = self.fn_ls[500:600]
Vtp = self.tp_ls[500:600]
V = self.trust_ls[500:600]

fig = plt.figure()

plt.bar(X, Yfp, label = 'fp', color = 'tab:orange')
plt.bar(X, Yfn, label = 'fn', color = 'tab:green')
plt.bar(X, Ytp, label = 'tp', color = 'tab:blue')
plt.plot(X, Y,  label = 'trust', color = 'tab:gray')
plt.fill_between(X, Y, color = 'tab:gray', alpha = 0.3)
plt.legend()
plt.title("Evolution of trust")
plt.xlabel("time(yrs)")
plt.ylabel("Damage, trust level")

path_data = os.getcwd() + '/../output/single_run/'
if not os.path.exists(path_data):
    os.makedirs(path_data)


#信頼のやつ。
fig.savefig(path_data + "loss_trust graph.png")

fig = plt.figure(figsize = (12, 5))

# 目盛り線方向を内側にする
plt.rcParams["xtick.direction"] = "in"
plt.rcParams["ytick.direction"] = "in"

# values defining
a1 = self.mc_stats.loc[:,'t_shock']
b = self.mc_stats.loc[:,'last_decay']
value = self.mc_stats.loc[:,'av_trust']

# mag_thr - half-time graph
ax1 = fig.add_subplot(1,3,1)

ax1_g = ax1.scatter(a1, b, c = value, cmap = 'RdYlBu_r', s = 2, vmin=0.0, vmax=1.0)
ax1.set_xlim(0.05, 0.40)
ax1.set_ylim(0.10, 0.60)
ax1.set_xlabel("trust_shock")
ax1.set_ylabel("trust_decay")

ax1.set_xticks([0.1,0.2,0.3,0.4])
ax1.set_yticks([0.1,0.2,0.3,0.4,0.5,0.6])

# color bar (relative loss)
cbar = plt.colorbar(ax1_g, ticks =[0.2, 0.4, 0.6, 0.8, 1.0])
cbar.set_label('average trust')

# save graph
path_data = os.getcwd() + '/../output/monte_carlo/revision/'
if not os.path.exists(path_data):
    os.makedirs(path_data)

fig.savefig(path_data + "Figure 6 (model efficiency).png")

gironsのみっつ並んだやつ大きくしたやつ。

        fig = plt.figure()

        # 目盛り線方向を内側にする
        plt.rcParams["xtick.direction"] = "in"
        plt.rcParams["ytick.direction"] = "in"

        # values defining
        a1 = self.mc_stats.loc[:,'prob_thr']
        b = self.mc_stats.loc[:,'rel_loss']
        value = self.mc_stats.loc[:,'fp']

        # mag_thr - half-time graph


        plt.scatter(a1, b, c = value, cmap = 'jet', s = 2, vmin=0.0, vmax=200)
        plt.xlim(0.10, 0.90)
        plt.ylim(0.50, 1.00)
        plt.xlabel("init_prob_thr")
        plt.ylabel("relative loss")

        plt.xticks([0.1,0.3,0.5,0.7,0.9])
        plt.yticks([0.50,0.60,0.70,0.80,0.90,1.00])

        # color bar (relative loss)
        cbar = plt.colorbar( ticks =[0.0, 50, 100, 150, 200])
        cbar.set_label('false positive')

        # save graph
        path_data = os.getcwd() + '/../output/monte_carlo/revision/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        fig.savefig(path_data + "Figure 6 (model efficiency).png")

# gironsのfigure 1。流出量。
