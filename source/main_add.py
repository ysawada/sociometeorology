# -*- coding: utf-8 -*-
###############################################################################
# PREP-FEWS
# Social Preparedness in Early Warning Systems model
#
# Main model script
#
# Marc Girons Lopez, Giuliano di Baldassarre, Jan Seibert
#
# Model is updated to consider Trust in FEWS
# by Yohei Sawada, Rin Kanai, Kotani Hitomu
###############################################################################

import numpy as np
import pandas as pd
from math import isclose

import modules as mod
import functions as fun
import os
import matplotlib.pyplot as plt

###############################################################################
# PREP-FEWS MODEL
###############################################################################


class PrepFews(mod.ForecastingSystem, mod.WarningAssessment,
               mod.SocialPreparedness, mod.LossEstimation, mod.RiskCognition,
               mod.ModelEvaluation, mod.TrustUpdate, mod.Prob_thrUpdate):
    """prep-fews model to ...
    """
    def __init__(self, mean, sd, shape, scale, mag_thr, init_prob_thr, dmg_thr,
                 dmg_shape, res_dmg, mit_cst, init_prep, shock, half_life,
                 prep_rate, init_trust, t_shock, t_decay, ms_params, fao_decay,
                 fao_params, ct_prep=False, ct_trust=False, ct_prob_thr=False):
        """
        """
        # set parameters and variables
        self.ct_prep = ct_prep
        self.preparedness = init_prep
        self.prep_rate = prep_rate

        #set trust
        self.ct_trust = ct_trust
        self.trust = init_trust

        #set mag_thr and prob_thr
        self.ct_prob_thr = ct_prob_thr
        self.prob_thr = init_prob_thr
        self.mag_thr = mag_thr

        # initialize super-classes
        mod.ForecastingSystem.__init__(self, mean, sd, shape, scale)
        #######mod.WarningAssessment.__init__(self, mag_thr, prob_thr)
        mod.LossEstimation.__init__(self, dmg_thr, dmg_shape, res_dmg, mit_cst)
        mod.SocialPreparedness.__init__(self, shock, half_life)
        mod.ModelEvaluation.__init__(self)

        # mod.TrustConstant.__init__(self, trust) #消してるだけ（trustの代入防止)
        mod.TrustUpdate.__init__(self, t_shock, t_decay, ms_params,
                                 fao_decay, fao_params)

        # Initialize lists to keep track of the states of the system
        self.forecast_ls = []
        self.outcome_ls = []
        self.climate_loss_ls = []
        self.warning_loss_ls = []
        self.warning_damage_ls = []
        self.preparedness_ls = []

        self.act_public_ls = []
        self.test_ls = []
        self.oc_tmp = []
        self.trust_ls = []
        self.prob_thr_ls = []
        self.fao_ls = []
        self.ms_ls = []
        self.risk_cog_ls = []


        #PrepFewsの__init__に入れるやつ。
        self.fp_ls = []
        self.fn_ls = []
        self.tp_ls = []
        #ここまで。

        # ロスの関数用。
        self.lloss_ls = []
        self.num_lloss_ls = []

        # keep track of the model results
        self.stats = None

    def run_model(self, datafile):
        """Run the prep-fews model

        Returns a dict witht the different performance measures
        """
        time_series = fun.load_data(datafile)

        for event in time_series['Runoff']:

            risk_cog = self.risk_cognition(self.preparedness,
                                           self.trust, self.prep_rate)
            # calculate losses if no forecasting system is operational
            climate_loss = self.estimate_damage(event)

            #trust→確率避難ではなくしたので#つけて消した。この考えで行くならそのうち削除。
            #act_public = self.if_prepare_ornot_update(self.trust)

            # calculate losses if a forecasting system is operational

            forecast = self.issue_forecast(event)
            warning_outcome = self.assess_warning_outcome(event, forecast,
                                                          self.mag_thr,
                                                          self.prob_thr)
            warning_damage = self.get_warning_damage(event, warning_outcome,
                                                     risk_cog)
            warning_loss = self.get_warning_loss(event, warning_outcome,
                                                 risk_cog)


            test_ifcheck = self.test(warning_outcome)
            oc_tmp = self.assess_warning_outcome(event, forecast, self.mag_thr,
                                                 self.prob_thr)

            # keep track of the states of the model
            self.forecast_ls.append(forecast)
            self.outcome_ls.append(warning_outcome)
            self.warning_loss_ls.append(warning_loss)
            self.climate_loss_ls.append(climate_loss)
            self.warning_damage_ls.append(warning_damage)
            self.preparedness_ls.append(self.preparedness)
            #trsut
            #self.act_public_ls.append(act_public)
            self.test_ls.append(test_ifcheck)

            self.oc_tmp = oc_tmp
            self.trust_ls.append(self.trust)
            self.prob_thr_ls.append(self.prob_thr)
            self.risk_cog_ls.append(risk_cog)


            #ここからPrepFewsのrunに挿入。
            if warning_outcome == 'false positive':
                self.fp_ls.append(warning_loss)

            else:
                self.fp_ls.append(0)

            if warning_outcome == 'false negative':
                self.fn_ls.append(warning_loss)

            else:
                self.fn_ls.append(0)

            if warning_outcome == 'true positive':
                self.tp_ls.append(warning_loss)

            else:
                self.tp_ls.append(0)

            #ここまでPrepFewsのrunに挿入。

            #閾値調整のために誤報数を確認したい。準備でもいいが。
            self.count_warning_outcomes(self.outcome_ls)
            fao = self.false_alarm_ratio()
            ms = self.missing_score()

            self.fao_ls.append(fao)
            self.ms_ls.append(ms)
            #ここまで。

            # 一定以上の大きさのlossをカウントしたい。
            if warning_loss >= 0.50:
                self.num_lloss_ls.append(warning_loss)

            if warning_loss >= 0.50:
                self.lloss_ls.append(warning_loss)
            else:
                self.lloss_ls.append(None)



            # シングルの時だけオンにする。
            #if event >= 0.6:
                #print(oc_tmp, round(self.trust, 3), round(event, 3), round(warning_loss, 3))

            #elif event < 0.6 and warning_loss >= 0.500:
                #print('exist', oc_tmp, round(event, 3))

            # まずは閾値操作は置いておく。もっと見るべきことがある。
            if self.ct_prob_thr is False:
                self.prob_thr = self.update_prob_thr(fao, ms, warning_loss,
                                                     oc_tmp, self.prob_thr)

            if self.ct_trust is False:
                self.trust = self.update_trust(climate_loss, warning_loss,
                                               self.trust, self.oc_tmp)

            # update the variables
            if self.ct_prep is False:
                self.preparedness = self.update_preparedness(warning_damage,
                                                             self.preparedness)




###############################################################################
# DEFINE THE SINGLE-RUN MODE
###############################################################################


class SingleRun(PrepFews):

    def __init__(self, configfile, ct_prep=False, ct_trust=False,
                 ct_prob_thr=False):
        """
        """
        params = fun.load_parameters(configfile)

        # set parameters and variables
        mean = params['Forecast']['acc_mean']
        sd = params['Forecast']['acc_sd']
        shape = params['Forecast']['prc_shape']
        scale = params['Forecast']['prc_scale']

        mag_thr = params['Warning']['mag_thr']
        init_prob_thr = params['Warning']['init_prob_thr']
        dmg_thr = params['Loss']['dmg_thr']
        dmg_shape = params['Loss']['dmg_shape']
        res_dmg = params['Loss']['res_dmg']
        mit_cst = params['Loss']['mit_cst']
        ct_prob_thr = ct_prob_thr

        ct_prep = ct_prep
        init_prep = params['Preparedness']['init_prep']
        shock = params['Preparedness']['shock']
        half_life = params['Preparedness']['half_life']
        prep_rate = params['Preparedness']['prep_rate']

        #experiment5 set trust(single)
        trust = params['Trust']['trust']
        init_trust = params['Trust']['init_trust']
        t_shock = params['Trust']['t_shock']
        t_decay = params['Trust']['t_decay']
        ms_params = params['Trust']['ms_params']
        fao_decay = params['Trust']['fao_decay']
        fao_params = params['Trust']['fao_params']
        ct_trust = ct_trust

        # initialize the model
        PrepFews.__init__(self, mean, sd, shape, scale, mag_thr, init_prob_thr,
                          dmg_thr, dmg_shape, res_dmg, mit_cst, init_prep,
                          shock, half_life, prep_rate, init_trust, t_shock,
                          t_decay, ms_params, fao_decay, fao_params,
                          ct_prep, ct_trust, ct_prob_thr)

        #filename_data, filename_data2は、
        #準備-損害の表現→'time_prep.txt'と'loss_prep.txt'
        #信頼-損害の表現は→'time_trust.txt'と'loss_trust.txt'にする。
    def run(self, datafile, statfile, filename_data = 'time_trust.txt',
            filename_data2 = 'loss_trust.txt'):
        """Perform a single run of the MEWS model
        """
        # run the model
        self.run_model(datafile)

        #ここからsingle_runに挿入。

        X = np.arange(500)
        Yfp = self.fp_ls
        Yfn = self.fn_ls
        Ytp = self.tp_ls
        Y = self.trust_ls

        Vll = self.lloss_ls[400:500]

        U = np.arange(400, 500)
        Vfp = self.fp_ls[400:500]
        Vfn = self.fn_ls[400:500]
        Vtp = self.tp_ls[400:500]
        V = self.trust_ls[400:500]

        figt = plt.figure()

        plt.bar(X, Yfp, label = 'fp', color = 'b', width=4.0)
        plt.bar(X, Yfn, label = 'fn', color = 'r', width=2.0)
        plt.bar(X, Ytp, label = 'tp', color = 'limegreen', width=2.0)
        plt.plot(X, Y,  label = 'trust', color = 'tab:gray', alpha = 0.3)
        plt.fill_between(X, Y, color = 'tab:gray', alpha = 0.3)
        plt.legend()
        plt.title("Evolution of trust")
        plt.xlabel("Time(yrs)")
        plt.ylabel("Damage, trust level")
        plt.ylim(0.0, 1.0)

        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        figt.savefig(path_data + "loss_trust graph.png")

        #ついでにpreparednessのグラフ描写も。（時間は上の X と U を変えないとダメ。
        Y1 = self.warning_loss_ls
        Y2 = self.preparedness_ls
        V1 = self.warning_loss_ls[400:500]
        V2 = self.preparedness_ls[400:500]


        figp = plt.figure()

        #plt.bar(U, Vfp, label = 'fp', color = 'tab:orange')
        #plt.bar(U, Vfn, label = 'fn', color = 'tab:green')
        #plt.bar(U, Vtp, label = 'tp', color = 'tab:blue')
        plt.bar(U, V1, color = "k", width = 0.2)
        plt.plot(U, V2, label = 'exp_level', color = 'tab:gray', alpha = 0.3)
        plt.fill_between(U, V2, color = 'tab:gray', alpha = 0.3)
        #plt.scatter(U, V1, s = 40, c = 'black')
        for i in range(len(U)):
            plt.scatter(U[i], V1[i], c = 'black', alpha = 1.0 if V1[i] > 0.0 else 0.0,
                        s = 40)
        plt.legend()
        plt.title("Evolution of Experience")
        plt.xlabel("Time(yrs)")
        plt.ylabel("Damage, Experience level")

        # save graph
        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        figp.savefig(path_data + "loss_exp graph.png")

        # prob_thrの動きを見たいグラフ。
        figpro = plt.figure()

        Y3 = self.prob_thr_ls
        Yfao = self.fao_ls
        Yms = self.ms_ls
        Yll = self.lloss_ls
        V3 = self.prob_thr_ls[400:500]
        Vfao = self.fao_ls[400:500]
        Vms = self.ms_ls[400:500]

        plt.bar(X, Yfp, label = 'fp', color = 'black', width=4.0)
        plt.bar(X, Yfn, label = 'fn', color = 'black', width=2.0)
        plt.bar(X, Ytp, label = 'tp', color = 'black', width=2.0)
        plt.plot(X, Y3, label = 'prob_thr', color = 'tab:gray', alpha = 0.3)
        plt.fill_between(X, Y3, color = 'tab:gray', alpha = 0.3)
        #for i in range(len(U)):
            #plt.scatter(U[i], V1[i], c = 'black', alpha = 1.0 if V1[i] > 0.49 else 0.0,
                        #s = 50)
        #plt.scatter(X, Yll, s = 40, c = 'black')
        plt.legend()
        plt.title("Evolution of probability threshold")
        plt.xlabel("Time(yrs)")
        plt.ylabel("Damage, prob_thr")
        plt.ylim(0.0, 1.0)

        # save graph
        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        figpro.savefig(path_data + "loss_prob_thr graph.png")

        #risk_cognitionの動きを見るグラフ。
        figris = plt.figure()

        Y4 = self.risk_cog_ls
        V4 = self.risk_cog_ls[400:500]

        plt.bar(X, Yfp, label = 'fp', color = 'tab:orange')
        plt.bar(X, Yfn, label = 'fn', color = 'tab:green')
        plt.bar(X, Ytp, label = 'tp', color = 'tab:blue')
        plt.plot(X, Y4, label = 'risk_cog', color = 'tab:gray', alpha = 0.3)
        plt.fill_between(X, Y4, color = 'tab:gray', alpha = 0.3)
        plt.legend()
        plt.title("Evolution of risk_cognition")
        plt.xlabel("time(yrs)")
        plt.ylabel("Damage, risk_cog")
        plt.ylim(0.0, 1.0)

        # save graph
        path_data = os.getcwd() + '/../output/single_run/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        figris.savefig(path_data + "loss_risk_cog graph.png")


        # save the model results
        self.calculate_statistics(self.outcome_ls, self.climate_loss_ls,
                                  self.warning_loss_ls, self.preparedness_ls,
                                  self.trust_ls, self.risk_cog_ls, self.num_lloss_ls,
                                  self.prob_thr_ls)



        self.save_statistics(statfile, mode='single_run')

###############################################################################
# DEFINE THE MONTE CARLO MODE
###############################################################################


class MonteCarloRun(PrepFews):

    def __init__(self, configfile, ct_prep=False, ct_trust=False,
                 ct_prob_thr=True):
        """
        """
        params = fun.load_parameters(configfile)

        # check if the config file is the correct one
        if len(params['Forecast']) != 8:
            raise ValueError('Are you sure you used a MonteCarlo config file?')

        # set parameters and variables
        self.iterations = params['MC']['iterations']

        self.mean_low = params['Forecast']['acc_mean_low']
        self.mean_high = params['Forecast']['acc_mean_high']
        self.sd_low = params['Forecast']['acc_sd_low']
        self.sd_high = params['Forecast']['acc_sd_high']
        self.shape_low = params['Forecast']['prc_shape_low']
        self.shape_high = params['Forecast']['prc_shape_high']
        self.scale_low = params['Forecast']['prc_scale_low']
        self.scale_high = params['Forecast']['prc_scale_high']

        self.mag_thr_low = params['Warning']['mag_thr_low']
        self.mag_thr_high = params['Warning']['mag_thr_high']
        self.init_prob_thr_low = params['Warning']['prob_thr_low']
        self.init_prob_thr_high = params['Warning']['prob_thr_high']
        self.ct_prob_thr = ct_prob_thr

        self.dmg_thr_low = params['Loss']['dmg_thr_low']
        self.dmg_thr_high = params['Loss']['dmg_thr_high']
        self.dmg_shape_low = params['Loss']['dmg_shape_low']
        self.dmg_shape_high = params['Loss']['dmg_shape_high']
        self.res_dmg_low = params['Loss']['res_dmg_low']
        self.res_dmg_high = params['Loss']['res_dmg_high']
        self.mit_cst_low = params['Loss']['mit_cst_low']
        self.mit_cst_high = params['Loss']['mit_cst_high']

        self.ct_prep = ct_prep
        self.init_prep_low = params['Preparedness']['init_prep_low']
        self.init_prep_high = params['Preparedness']['init_prep_high']
        self.shock_low = params['Preparedness']['shock_low']
        self.shock_high = params['Preparedness']['shock_high']
        self.half_life_low = params['Preparedness']['half_life_low']
        self.half_life_high = params['Preparedness']['half_life_high']
        self.prep_rate_low = params['Preparedness']['prep_rate_low']
        self.prep_rate_high = params['Preparedness']['prep_rate_high']

        #trust
        self.ct_trust = ct_trust
        self.init_trust_low = params['Trust']['init_trust_low']
        self.init_trust_high = params['Trust']['init_trust_high']
        self.t_shock_low = params['Trust']['t_shock_low']
        self.t_shock_high = params['Trust']['t_shock_high']
        self.t_decay_low= params['Trust']['t_decay_low']
        self.t_decay_high = params['Trust']['t_decay_high']
        self.ms_params_low = params['Trust']['ms_params_low']
        self.ms_params_high = params['Trust']['ms_params_high']
        self.fao_decay_low = params['Trust']['fao_decay_low']
        self.fao_decay_high = params['Trust']['fao_decay_high']
        self.fao_params_low = params['Trust']['fao_params_low']
        self.fao_params_high = params['Trust']['fao_params_high']

        #end

        columns = ['index', 'mean', 'sd', 'shape', 'scale', 'mag_thr',
                   'prob_thr', 'dmg_thr', 'dmg_shape', 'res_dmg', 'mit_cst',
                   'init_prep', 'shock', 'half_life', 'prep_rate', 'init_trust',
                   't_shock', 't_decay', 'ms_params', 'flood_freq', 'yrp', 'hr',
                   'far', 'fao', 'ts', 'rel_loss', 'av_prep', 'av_trust',
                   'av_risk_cog', 'av_prob_thr', 'num_lloss', 'tn', 'fp', 'fn',
                   'tp']

        self.mc_stats = pd.DataFrame(index=range(self.iterations),
                                     columns=columns)



    def mc_value(self, val_low, val_high):
        """
        """
        # if no range is given (val_low = val_high), pick that value
        if isclose(val_low, val_high) is True:
            return val_low
        # return a random value between val_low and val_high
        else:
            return val_low + np.random.random() * (val_high - val_low)

    def run(self, datafile, statfile):
        """
        """
        fun.progress_bar(0, self.iterations)
        for i in range(self.iterations):

            # set the random values
            mean = self.mc_value(self.mean_low, self.mean_high)
            sd = self.mc_value(self.sd_low, self.sd_high)
            shape = self.mc_value(self.shape_low, self.shape_high)
            scale = self.mc_value(self.scale_low, self.scale_high)
            mag_thr = self.mc_value(self.mag_thr_low, self.mag_thr_high)
            init_prob_thr = self.mc_value(self.init_prob_thr_low, self.init_prob_thr_high)
            dmg_thr = self.mc_value(self.dmg_thr_low, self.dmg_thr_high)
            dmg_shape = self.mc_value(self.dmg_shape_low, self.dmg_shape_high)
            res_dmg = self.mc_value(self.res_dmg_low, self.res_dmg_high)
            mit_cst = self.mc_value(self.mit_cst_low, self.mit_cst_high)
            init_prep = self.mc_value(self.init_prep_low, self.init_prep_high)
            shock = self.mc_value(self.shock_low, self.shock_high)
            half_life = self.mc_value(self.half_life_low, self.half_life_high)
            prep_rate = self.mc_value(self.prep_rate_low, self.prep_rate_high)
            init_trust = self.mc_value(self.init_trust_low, self.init_trust_high)
            t_shock = self.mc_value(self.t_shock_low, self.t_shock_high)
            t_decay = self.mc_value(self.t_decay_low, self.t_decay_high)
            ms_params = self.mc_value(self.ms_params_low, self.ms_params_high)
            fao_decay = self.mc_value(self.fao_decay_low, self.fao_decay_high)
            fao_params = self.mc_value(self.fao_params_low, self.fao_params_high)


            # set the model
            # initialize the model
            PrepFews.__init__(self, mean, sd, shape, scale, mag_thr, init_prob_thr,
                          dmg_thr, dmg_shape, res_dmg, mit_cst, init_prep,
                          shock, half_life, prep_rate, init_trust, t_shock,
                          t_decay, ms_params, fao_decay, fao_params,
                          self.ct_prep, self.ct_trust, self.ct_prob_thr)

#            model = PrepFews(mean, sd, shape, scale, mag_thr, init_prob_thr, # Y.Saw 20210312
#                             dmg_thr, dmg_shape, res_dmg, mit_cst, init_prep,
#                             shock, half_life, prep_rate, init_trust, t_shock,
#                             t_decay, ms_params, fao_decay, fao_params,
#                             self.ct_prep, self.ct_trust, self.ct_prob_thr)

            # run the model
            #model.run_model(datafile)
            self.run_model(datafile)
            
            # all timeseries are dumped by Y.Saw 20210312
            fignum = i
            dataens = np.zeros((1000,6))
            dataens[:,0] = self.preparedness_ls
            dataens[:,1] = self.trust_ls
            dataens[:,2] = self.risk_cog_ls
            dataens[:,3] = self.fp_ls
            dataens[:,4] = self.fn_ls
            dataens[:,5] = self.tp_ls

            path_data = os.getcwd() + '/../output/monte_carlo/revision/'
            if not os.path.exists(path_data):
                os.makedirs(path_data)
            ensfilename = 'timeseries'+str(fignum)
            np.savetxt(path_data + ensfilename,dataens)


            # calculate the model statistics
            self.calculate_statistics(self.outcome_ls,
                                       self.climate_loss_ls,
                                       self.warning_loss_ls,
                                       self.preparedness_ls,
                                       self.trust_ls,
                                       self.risk_cog_ls,
                                       self.num_lloss_ls,
                                       self.prob_thr_ls)


            # store the results
            self.mc_stats.loc[i] = [i, mean, sd, shape, scale, mag_thr,
                                    init_prob_thr, dmg_thr, dmg_shape, res_dmg,
                                    mit_cst, init_prep, shock, half_life,
                                    prep_rate, init_trust, t_shock, t_decay,
                                    ms_params,
                                    self.stats['Probability of occurrence'],
                                    self.stats['Return period (yrs)'],
                                    self.stats['Hit rate'],
                                    self.stats['False alarm rate'],
                                    self.stats['False alarm ratio'],
                                    self.stats['Threat score'],
                                    self.stats['Relative loss'],
                                    self.stats['Average preparedness'],
                                    self.stats['Average trust'],
                                    self.stats['Average risk cognition'],
                                    self.stats['Average probability threshold'],
                                    self.stats['Number of large loss'],
                                    self.stats['true negative'],
                                    self.stats['false positive'],
                                    self.stats['false negative'],
                                    self.stats['true positive']]

            # update progress bar
            fun.progress_bar(i, self.iterations)

        # experiment2 add graph(monte_carlo run)

        fig = plt.figure(figsize = (8,4))

        # 目盛り線方向を内側にする
        plt.rcParams["xtick.direction"] = "in"
        plt.rcParams["ytick.direction"] = "in"

        # values defining
        a1 = self.mc_stats.loc[:,'prob_thr']
        b = self.mc_stats.loc[:,'rel_loss']
        value = self.mc_stats.loc[:,'num_lloss']

        # mag_thr - half-time graph


        plt.scatter(a1, b, c = value, cmap = 'seismic', s = 2, vmin=0.0, vmax=5)
        plt.xlim(0.10, 0.90)
        plt.ylim(0.00, 1.20)
        plt.xlabel("init_prob_thr")
        plt.ylabel("relative loss")

        plt.xticks([0.1,0.3,0.5,0.7,0.9])
        plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0,1.2])

        # color bar (relative loss)
        cbar = plt.colorbar( ticks =[0.0, 1.0, 2.0, 3.0, 4.0, 5.0])
        cbar.set_label('large loss')

        # save graph
        path_data = os.getcwd() + '/../output/monte_carlo/revision/'
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        fig.savefig(path_data + "Figure 6 (model efficiency).png")
        # figure 6 test

        # end

        # write the results to the output file
        self.save_statistics(statfile, mode='monte_carlo')

        # finalize progress bar
        fun.progress_bar(self.iterations, self.iterations)

###############################################################################
# RUN THE PREP-FEWS MODEL
###############################################################################

if __name__ == '__main__':

    model_type = 'monte_carlo'

    if model_type == 'single_run':
        single_run = SingleRun(configfile='config.yml')
        single_run.run(datafile='time_series.txt',
                       statfile='results_single.txt')

    elif model_type == 'monte_carlo':
        np.random.seed(seed=14) # Y.Saw 20210325
        monte_carlo = MonteCarloRun(configfile='experiment1.yml')
        monte_carlo.run(datafile='time_series.txt',
                        statfile='exp1_timeseries.txt')
